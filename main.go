package main

import (
	// "bytes"
	"context"
	"encoding/json"
	"log"
	// "strconv"
	"strings"
	// "sync"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
)

func main() {
	elasticInsertTest()
	elasticSearchTest()
	// elasticDeleteTest()
	
}

func elasticInsertTest() {
	// Initialize a client with the default settings.
	//
	// An `ELASTICSEARCH_URL` environment variable will be used when exported.
	//
	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	// 2. Index documents concurrently
	//

	// Build the request body.
	var b strings.Builder
	b.WriteString(`{"title" : "`)
	b.WriteString("test1")
	b.WriteString(`",`)
	// b.WriteString(`"}`)
	b.WriteString(`"name" : "`)
	b.WriteString("sombun3")
	b.WriteString(`"}`)
	// var jsonString string = `{"title": "create", "name": "eiei"}`

	var jsonString string = `{` +
	`"title":` + `"what",` + 
	`"name":` + `"eiei"` +
	`"surname":` + `"yoyo"` +
	`}`

	// Set up the request object.
	req := esapi.IndexRequest{
		Index:      "t1",
		// DocumentID: strconv.Itoa(i + 1),
		// DocumentID: "O2oAkHcBd3m-CPd-cgfd",
		// Body:       strings.NewReader(b.String()),
		Body:       strings.NewReader(jsonString),
		Refresh:    "true",
	}

	log.Printf("test1 %s", req)

	// Perform the request with the client.
	res, err := req.Do(context.Background(), es)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		log.Printf("[%s] Error indexing document", res.Status())
	} else {
		// Deserialize the response into a map.
		var r map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
			log.Printf("Error parsing the response body: %s", err)
		} else {
			// Print the response status and indexed document version.
			log.Printf("%s;", r)
			log.Printf("[%s] %s; ID=%s; version=%d", res.Status(), r["result"], r["_id"],int(r["_version"].(float64)))
		}
	}

	log.Println(strings.Repeat("-", 37))
}

func elasticSearchTest() {
	// Initialize a client with the default settings.
	//
	// An `ELASTICSEARCH_URL` environment variable will be used when exported.
	//
	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	// 2. Index documents concurrently
	//

	// Build the request body.

	var index []string
	var sort []string
	index = append(index, "t1")
	index = append(index, "my_index")
	sort = append(sort, "age:desc")
	query := "age:{10 TO 30}"
	size := 100
	from := 0

	// Set up the request object.
	req := esapi.SearchRequest{
		Index:      index,
		Query:		query,
		Size:		&size,
		From:		&from,
		Sort: 		sort,

	}

	// Perform the request with the client.
	res, err := req.Do(context.Background(), es)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		log.Printf("[%s] Error indexing document", res.Status())
	} else {
		// Deserialize the response into a map.
		var r map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
			log.Printf("Error parsing the response body: %s", err)
		} else {
			log.Printf(
				"[%s] %d hits; took: %dms",
				res.Status(),
				int(r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)),
				int(r["took"].(float64)),
			)
			// Print the ID and document source for each hit.
			for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
				log.Printf(" * Index=%s, ID=%s, %s", hit.(map[string]interface{})["_index"], hit.(map[string]interface{})["_id"], hit.(map[string]interface{})["_source"])
			}
		}
	}

	log.Println(strings.Repeat("-", 37))
}

func elasticDeleteTest() {
	// Initialize a client with the default settings.
	//
	// An `ELASTICSEARCH_URL` environment variable will be used when exported.
	//
	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	// 2. Index documents concurrently
	//

	// Build the request body.

	index := "t1"
	documentID := "OWr9j3cBd3m-CPd-Awfm"

	// Set up the request object.
	req := esapi.DeleteRequest{
		Index:      	index,
		DocumentID:		documentID,
	}

	// log.Printf("test1 %s", req)

	// Perform the request with the client.
	res, err := req.Do(context.Background(), es)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		log.Printf("[%s] Error indexing document", res.Status())
	} else {
		// Deserialize the response into a map.
		var r map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
			log.Printf("Error parsing the response body: %s", err)
		} else {
			log.Printf("[%s], Deleted ID: %s, success", res.Status, documentID)
			log.Printf("[%s]", r["result"])
		}
	}

	log.Println(strings.Repeat("-", 37))
}


func keepImport() {

}